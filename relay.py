#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    relay.py
#    Part of "Raspberry Pi IoT: Temperature and Humidity controller"
#    Copyright 2016 Pavlos Iliopoulos, techprolet.com
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


class Relay:
    import time
    import RPi.GPIO as GPIO
 
    # be sure you are setting pins accordingly
    # GPIO21
    RelayPin = 21
    
    
     
    # Define some settings
    RelayWaitTime = 10
    
    RelayIsOn = False
    
    def __init__(self, doCleanup = True):
        if doCleanup:
            self.GPIO.cleanup() #cleaning up in case GPIOS have been preactivated
         
            # Use BCM GPIO references
            # instead of physical pin numbers
            self.GPIO.setmode(self.GPIO.BCM)
            
        self.GPIO.setup(self.RelayPin,self.GPIO.OUT)
        self.GPIO.output(self.RelayPin, False)
    
        #wait some time to start
        self.time.sleep(0.5)
    
     
    # Power relay on
    def Start(self):
        try:
            self.GPIO.output(self.RelayPin, True)
            RelayIsOn = True
        except:
            self.GPIO.cleanup();
            raise
        
    # Power relay off
    def Stop(self):
        try:
            self.GPIO.output(self.RelayPin, False)
            RelayIsOn = False
        except:
            self.GPIO.cleanup();
            raise
    
        
    def Uninitialize(self):
        print "Uninitializing relay..."
        self.GPIO.cleanup();
        self.GPIO.setmode(self.GPIO.BCM)
        
        self.GPIO.setup(self.RelayPin,self.GPIO.OUT)
        self.GPIO.output(self.RelayPin, False)
        print "...done."
