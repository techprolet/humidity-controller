#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    stepper.py
#    Part of "Raspberry Pi IoT: Temperature and Humidity controller"
#    Copyright 2016 Pavlos Iliopoulos, techprolet.com
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Stepper:
    import sys, time
    import RPi.GPIO as GPIO
        
    # be sure you are setting pins accordingly
    # GPIO9,GPIO25, GPIO8, GPIO7
    StepPins = [9, 25, 26, 20]
     
    #wait some time to start
    time.sleep(0.5)
     
    # Define some settings
    WaitTime = 0.0015
     
    #Bipolar stepper sequence
    PinCount = 4
    
    Seq = [3,2,1,0]
    Seq[0] = [0,1,1,0]
    Seq[1] = [0,1,0,1]
    Seq[2] = [1,0,0,1]
    Seq[3] = [1,0,1,0]
    
    def __init__(self, doCleanup = True):
        if doCleanup:
            self.GPIO.cleanup() #cleaning up in case GPIOS have been preactivated
         
            # Use BCM GPIO references
            # instead of physical pin numbers
            self.GPIO.setmode(self.GPIO.BCM)
     
        # Set all pins as output
        for pin in self.StepPins:
            self.GPIO.setup(pin,self.GPIO.OUT)
            self.GPIO.output(pin, False)
    
    def MoveStepper(self, steps, direction):
        print "Moving stepper", steps, "steps, direction", direction, "(open)" if direction == 1 else "(close)"
        
        stepCounter = 0
                
        # Start main loop
        try:
            i = 0
            while i < steps:
                for pin in range(0, 4):
                    xpin = self.StepPins[pin]
                    if self.Seq[stepCounter][pin] != 0:
                        self.GPIO.output(xpin, True)
                    else:
                        self.GPIO.output(xpin, False)
                if direction == 1:
                    stepCounter += 1
                else:
                    stepCounter -= 1
                    
                # If we reach the end of the sequence
                # start again
                if (stepCounter == self.PinCount):
                    stepCounter = 0
                if (stepCounter < 0):
                    stepCounter = self.PinCount - 1
                
                i += 1
                # Wait before moving on
                self.time.sleep(self.WaitTime)
        except:
            print "Stepper exception", self.sys.exc_info()[0]
            self.GPIO.cleanup();
            raise

    def Uninitialize(self):
        print "Uninitializing stepper..."
        self.GPIO.cleanup();
        self.GPIO.setmode(self.GPIO.BCM)
        for pin in self.StepPins:
            self.GPIO.setup(pin,self.GPIO.OUT)
            self.GPIO.output(pin, False)
        print "...done."