#!/bin/sh
# humidity_controller.sh
# run the humidity controller program


# to install do:
# chmod 755 humidity_controller.sh
# mkdir logs
# sudo crontab -e
# and enter the following line
# @reboot sh /home/pi/humidity-controller/humidity_controller.sh >/home/pi/logs/cronlog 2>&1
cd /home/pi/humidity-controller
sudo python /home/pi/humidity-controller/humidity_controller.py
