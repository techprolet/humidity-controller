#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    humidity_controller.py
#    Part of "Raspberry Pi IoT: Temperature and Humidity controller"
#    Copyright 2016 Pavlos Iliopoulos, techprolet.com
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import math
import stepper
import relay
from time import sleep, strftime

import Adafruit_DHT
from ubidots import ApiClient
from ubidots import UbidotsError400, UbidotsForbiddenError, UbidotsInvalidInputError


ControllerIniFile = "humidity_controller.ini"
ApiIniFile = "ubidots_api.ini"

ThresholdWindowOpen = 62.0
ThresholdWindowClose = 58.0
ThresholdFanStart = 72.0
ThresholdFanStop = 68.0

WindowSteps = 15000

windowOpen = False
fanRunning = False
windowOpener = 0
fanRelay = 0

execfile('./settings_sensor.py')

bufferSize = 20



api = 0
tempVar = 0
humidVar = 0
windowVar = 0
fanVar = 0

#upload to ubidots every 6 measurements (free plan compatible)
uploadEvery = 6
iterationCounter = 0


lastVals = []
lastValIndex = 0



def Initialize():
	global windowOpen, windowOpener, fanRelay
	
	# sleep for ten seconds
	sleep(10)
	windowOpener = stepper.Stepper()
	fanRelay = relay.Relay(False)
	
	# sleep for ten seconds
	sleep(10)
	
	apiId = ""
	tempVarId = ""
	humidVarId = ""
	windowVarId = ""
	fanVarId = ""
	
	try:
		print "Reading ubidots api initialization file..."
		apiHandle = open(ApiIniFile, "r")
		while True:                            # Keep reading forever
			line = apiHandle.readline()   # Try to read next line
			if len(line) == 0:              # If there are no more lines
				break                          #     leave the loop
			else:
				line = line.split()
				if len(line) == 2:
					if line[0] == "ClientId":
						apiId = line[1]
					elif line[0] == "TemperatureVar":
						tempVarId = line[1]
					elif line[0] == "HumidityVar":
						humidVarId = line[1]
					elif line[0] == "WindowVar":
						windowVarId = line[1]
					elif line[0] == "FanVar":
						fanVarId = line[1]
		apiHandle.close()
		print "...done."
		LoadApi(apiId, tempVarId, humidVarId, windowVarId, fanVarId)
	except:
		e = sys.exc_info()[0]
		print "Reading ubidots api initialization file failed with exception", e, "... will try to connect again later."
	
	
	try:
		print "Reading controller initialization file..."
		iniHandle = open(ControllerIniFile, "r")
		while True:                            # Keep reading forever
			line = iniHandle.readline()   # Try to read next line
			if len(line) == 0:              # If there are no more lines
				break                          #     leave the loop
			else:
				line = line.split()
				if line[0] == "WindowOpen" and line[1].lower() == "true":
					windowOpen = True
					print "Window state was set to open."
		iniHandle.close()
		print "...done."
	except:
		e = sys.exc_info()[0]
		print "Reading controller initialization file failed with exception", e, "... will assume window is closed."

def LoadApi(apiId, tempVarId, humidVarId, windowVarId, fanVarId):
	global api, tempVar, humidVar, windowVar, fanVar
	try:
		#Create an "API" object
		api = ApiClient(apiId)
	
		#Create a "Variable" object
		tempVar = api.get_variable(tempVarId)
		humidVar = api.get_variable(humidVarId)
		windowVar = api.get_variable(windowVarId)
		fanVar = api.get_variable(fanVarId)
	except:
		e = sys.exc_info()[0]
		print "Loading ubidots api failed with exception",e,"... will retry later."
		api=0

def UpdateIniFile():
	global windowOpen, ControllerIniFile
	windowStateLine = 'WindowOpen {0}\n'.format(str(windowOpen))
	print "Will now attempt to update the ini file"
	print windowStateLine
	try:
		iniHandle = open(ControllerIniFile, "w")
		iniHandle.write(windowStateLine)
		iniHandle.close()
		print "...done."
	except:
		e = sys.exc_info()[0]
		print "Failed updating the ini file with exception",e,"... will retry later."
		
	
def uploadToCloud(sensorVar, sensorVal, sensorName = ""):
	print "Uploading",sensorName, "to cloud ("+str(sensorVal)+")"
	if (api == 0):
				Initialize()
	if (api != 0):
		try:
			sensorVar.save_value({'value':sensorVal})
			print "...done"
		except UbidotsError400 as e:
			print "General Description: %s; and the detail: %s" % (e.message, e.detail)
		except UbidotsForbiddenError as e:
			print "For some reason my account does not have permission to read this variable."
			print "General Description: %s; and the detail: %s" % (e.message, e.detail)
		except UbidotsInvalidInputError as e:
			print "General Description: %s; and the detail: %s" % (e.message, e.detail)
		except:
			e = sys.exc_info()[0]
			print "Exception while connecting to Ubidots:", e

Initialize()

UpdateIniFile()
uploadToCloud(windowVar, 5.0 if windowOpen else 0.0, "window state")
uploadToCloud(fanVar, 10.0 if fanRunning else 0.0, "fan state")


try:
	while True:
		humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
		
		if humidity is not None and temperature is not None:
			print strftime("%Y-%m-%d %H:%M:%S"),'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)
			
			#initialize chart
			if len(lastVals) == 0:
				for i in range(0,bufferSize):
					lastVals.append(humidity)
				
			lastVals[lastValIndex] = humidity
			lastValIndex = (lastValIndex +1) % bufferSize
			
			if humidity > ThresholdWindowOpen and windowOpen == False:
				"Opening window..."
				windowOpener.MoveStepper(WindowSteps, 1)
				windowOpen = True
				iterationCounter = 0
				UpdateIniFile()
			elif humidity < ThresholdWindowClose and windowOpen == True:
				"Closing window..."
				windowOpener.MoveStepper(WindowSteps, 0)
				windowOpen = False
				iterationCounter = 0
				UpdateIniFile()
			elif humidity > ThresholdFanStart and fanRunning == False:
				"Starting fan..."
				fanRelay.Start()
				fanRunning = True
				iterationCounter = 0
			elif humidity < ThresholdFanStop and fanRunning == True:
				"Stopping fan..."
				fanRelay.Stop()
				fanRunning = False
				iterationCounter = 0
	
			if (iterationCounter % uploadEvery == 0):
				iterationCounter = 1
				uploadToCloud(tempVar, temperature, "temperature")
				uploadToCloud(humidVar, humidity, "humidity")
				uploadToCloud(windowVar, 5.0 if windowOpen else 0.0, "window state")
				uploadToCloud(fanVar, 10.0 if fanRunning else 0.0, "fan state")
			else:
				iterationCounter += 1
			
		else:
			print 'Sensor reading failed. Will try again in the next cycle'
		
		
		
		# sleep for ten seconds
		sleep(10)
		
		
except:
	windowOpener.Uninitialize()
	fanRelay.Uninitialize()
finally:
	windowOpener.Uninitialize()
	fanRelay.Uninitialize()
